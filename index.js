const http = require('http');

const cats = [
    {
        name: 'meow1',
        age: 1,
        breed: 'cat'
    }
]

const requestListener = function (req, res) {
  res.writeHead(200);
  res.end(JSON.stringify(cats));
}

const server = http.createServer(requestListener);
server.listen(3000, "0.0.0.0");