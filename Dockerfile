FROM node:16.9.0

WORKDIR /home/usr/app

COPY . .

EXPOSE 3000

CMD ["npm", "run", "start"]